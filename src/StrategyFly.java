public class StrategyFly implements MoveStrategy {

    @Override
    public void move() {
        System.out.println("Hero is flying");
    }

}
