public class Hero {
    private MoveStrategy moveStrategy;

    public Hero() {
        moveStrategy = new StrategyWalk();
    }

    void changeMovingStrategy(MoveStrategy strat) {
        try {
            if (strat == null) {
                throw new NullPointerException("Error: null pointer given");
            }
            moveStrategy = strat;
        } catch (NullPointerException e) {
            System.err.println(e.getMessage());
        }
    }

    void move() {
        moveStrategy.move();
    }

}
