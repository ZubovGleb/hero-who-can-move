
public class StrategyWalk implements MoveStrategy {

    @Override
    public void move() {
        System.out.println("Hero is walking");
    }

}
