import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Hero hero = new Hero();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Choose the desired action");
        System.out.println("    0) Exit");
        System.out.println("    1) Select Walking strategy");
        System.out.println("    2) Select Riding a horse strategy");
        System.out.println("    3) Select Flying strategy");
        System.out.println("    4) Perform a move");
        int choice = 1;
        while (choice != 5) {
            if (scanner.hasNextInt()) {
                choice = scanner.nextInt();
            } else {
                System.err.println("Invalid data. En ter something correct");
                scanner.next();
                continue;
            }
            switch (choice) {
                case 0 -> System.out.println("End of the program...");
                case 1 -> {
                    hero.changeMovingStrategy(new StrategyWalk());
                    hero.move();
                }
                case 2 -> {
                    hero.changeMovingStrategy(new StrategyRideAHorse());
                    hero.move();
                }
                case 3 -> {
                    hero.changeMovingStrategy(new StrategyFly());
                    hero.move();
                }
                case 4 -> hero.move();
                default -> System.err.println("Invalid data. Enter something correct");
            }
        }
        scanner.close();
    }
}
