public class StrategyRideAHorse implements MoveStrategy {

    @Override
    public void move() {
        System.out.println("Hero is riding a horse");
    }

}
